import React, { FC, Fragment } from "react";
import { Grid } from "@material-ui/core";

import Card, { cardProps } from "../02_molecules/Card";

export type cardsProps = {
  people: Array<cardProps>;
};

const Cards: FC<cardsProps> = ({ people }) => {
  return (
    <Fragment>
      {people.map((person) => (
        <Grid item>
          <Card
            name={person.name}
            sex={person.sex}
            age={person.age}
            text={person.text}
          />
        </Grid>
      ))}
    </Fragment>
  );
};

export default Cards;
