import React, { FC, Fragment } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import Home from "../../containers/Home";
import Matching from "../../containers/Matching";

export const path = {
  home: "/",
  matching: "/register",
};

const App: FC = () => {
  return (
    <Fragment>
      <Switch>
        <Route exact path={path.home} component={Home} />
        <Route exact path={path.matching} component={Matching} />
        <Redirect to={path.home} />
      </Switch>
    </Fragment>
  );
};

export default App;
