import React, { FC, Fragment } from "react";
import styled from "styled-components";
import { Grid, Container, Button } from "@material-ui/core";

import Cards, { cardsProps } from "../03_organisms/Cards";

export type homeProps = cardsProps;

const Home: FC<homeProps> = ({ people }) => {
  return (
    <Fragment>
      <Container maxWidth="md">
        <Styled.RootGrid container direction="column" justify="center">
          <Styled.MenuGrid item container justify="center" alignItems="center">
            <Button variant="contained" color="primary">
              ADD
            </Button>
          </Styled.MenuGrid>
          <Styled.ContentsGrid
            item
            container
            direction="row"
            spacing={3}
            justify="center"
          >
            <Cards people={people} />
          </Styled.ContentsGrid>
        </Styled.RootGrid>
      </Container>
    </Fragment>
  );
};

export default Home;

const Styled = {
  RootGrid: styled(Grid)`
    width: 100%;
  `,
  MenuGrid: styled(Grid)`
    height: 100px;
  `,
  ContentsGrid: styled(Grid)``,
};
