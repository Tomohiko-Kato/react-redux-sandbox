import React, { FC, Fragment } from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { Button, Container, Grid } from "@material-ui/core";

import { path } from "./App";
import success_img from "../../images/success.png";
import failure_img from "../../images/failure.png";

export type matchingProps = {
  success: boolean;
};

const Matching: FC<matchingProps> = ({ success }) => {
  const history = useHistory();

  return (
    <Fragment>
      <Container maxWidth="md">
        <Styled.RootGrid container>
          <Styled.ImgGrid item container justify="center" alignItems="center">
            {success ? (
              <Styled.Img src={success_img} alt="success" />
            ) : (
              <Styled.Img src={failure_img} alt="failure" />
            )}
          </Styled.ImgGrid>
          <Styled.FooterGrid
            item
            container
            justify="center"
            alignItems="center"
          >
            <Button
              variant="contained"
              color="primary"
              onClick={() => history.push(path.home)}
            >
              Back
            </Button>
          </Styled.FooterGrid>
        </Styled.RootGrid>
      </Container>
    </Fragment>
  );
};

export default Matching;

const Styled = {
  RootGrid: styled(Grid)`
    width: 100%;
    height: 100vh;
  `,
  ImgGrid: styled(Grid)`
    width: 100%;
    height: 70%;
  `,
  FooterGrid: styled(Grid)`
    width: 100%;
    height: 30%;
  `,
  Img: styled.img`
    width: 100%;
    height: 100%;
  `,
};
