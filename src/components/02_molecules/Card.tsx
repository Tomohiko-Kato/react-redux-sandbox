import React, { FC, Fragment } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import {
  Divider,
  Typography,
  Grid,
  Paper,
  Avatar,
  Button,
} from "@material-ui/core";

import { path } from "../04_templates/App";

export type cardProps = {
  name: string;
  sex: string;
  age: number;
  text: string;
};

const Card: FC<cardProps> = ({ name, sex, age, text }) => {
  const history = useHistory();
  return (
    <Fragment>
      <Styled.Card elevation={3}>
        <Styled.RootGrid container justify="center">
          <Styled.HeaderGrid container alignItems="center">
            <Styled.FigGrid item>
              <Styled.Avatar color="primary">{name[0]}</Styled.Avatar>
            </Styled.FigGrid>
            <Styled.StatusGrid item>
              <Styled.NameGrid item container alignItems="center">
                <Typography>{"名前：" + name}</Typography>
              </Styled.NameGrid>
              <Styled.AgeGrid item>{"年齢：" + age}</Styled.AgeGrid>
              <Styled.SexGrid item>{"性別：" + sex}</Styled.SexGrid>
            </Styled.StatusGrid>
          </Styled.HeaderGrid>
          <Divider />
          <Styled.BodyGrid item container justify="center" alignItems="center">
            {text}
          </Styled.BodyGrid>
          <Divider />
          <Styled.FooterGrid
            container
            item
            justify="center"
            alignItems="center"
          >
            <Styled.Button
              variant="contained"
              color="primary"
              onClick={() => history.push(path.matching)}
            >
              Match
            </Styled.Button>
          </Styled.FooterGrid>
        </Styled.RootGrid>
      </Styled.Card>
    </Fragment>
  );
};

export default Card;

const Styled = {
  Card: styled(Paper)`
    width: 250px;
    height: 250px;
  `,
  RootGrid: styled(Grid)`
    width: 100%;
    height: 100%;
  `,
  HeaderGrid: styled(Grid)`
    width: 100%;
    height: 40%;
  `,
  BodyGrid: styled(Grid)`
    width: 80%;
    height: 40%;
  `,
  FooterGrid: styled(Grid)`
    width: 100%;
    height: 20%;
  `,
  FigGrid: styled(Grid)`
    width: 40%;
    align-items: center;
  `,
  StatusGrid: styled(Grid)`
    width: 60%;
    height: 100%;
  `,
  NameGrid: styled(Grid)`
    height: 40%;
  `,
  AgeGrid: styled(Grid)`
    height: 30%;
  `,
  SexGrid: styled(Grid)`
    height: 30%;
  `,
  Avatar: styled(Avatar)`
    width: 70px;
    height: 70px;
    margin: 0 auto;
  `,
  Button: styled(Button)``,
};
