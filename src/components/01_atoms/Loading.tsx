import React, { FC, Fragment } from "react";
import styled from "styled-components";
import { CircularProgress, Grid } from "@material-ui/core";

const Loading: FC = () => {
  return (
    <Fragment>
      <Styled.RootGrid container justify="center" alignItems="center">
        <CircularProgress />
      </Styled.RootGrid>
    </Fragment>
  );
};

export default Loading;

export const Styled = {
  RootGrid: styled(Grid)`
    width: 100vw;
    height: 100vh;
  `,
};
