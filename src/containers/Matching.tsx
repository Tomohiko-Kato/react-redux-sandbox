import React, { FC, Fragment, useEffect, useState } from "react";

import MatchingComponent from "../components/04_templates/Matching";
import Loading from "../components/01_atoms/Loading";

const Matching: FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const judgeSuccess = () => {
    const val = Math.random();
    if (val > 0.5) {
      return true;
    } else {
      return false;
    }
  };

  const sleep = (waitMsec: number) => {
    return new Promise((r) => setTimeout(r, waitMsec));
  };

  useEffect(() => {
    const fakeLoad = async (waitMsec: number) => {
      await sleep(waitMsec);
      setIsLoading(false);
    };
    fakeLoad(3000);
  }, []);

  return (
    <Fragment>
      {isLoading ? <Loading /> : <MatchingComponent success={judgeSuccess()} />}
    </Fragment>
  );
};

export default Matching;
