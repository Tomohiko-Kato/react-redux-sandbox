import React, { FC } from "react";

import AppComponent from "../components/04_templates/App";

const App: FC = () => {
  return <AppComponent />;
};

export default App;
