import React, { FC, Fragment } from "react";

import HomeComponent from "../components/04_templates/Home";

const people = [
  { name: "加藤知彦", sex: "男", age: 24, text: "髪の毛切りに行きたい！！" },
  {
    name: "中山篤志",
    sex: "男",
    age: 24,
    text: "二郎以外はラーメンじゃない！！",
  },
  { name: "岩淵直哉", sex: "男", age: 22, text: "秒で復縁しました！！" },
  { name: "古川大吾", sex: "男", age: 24, text: "PC持って帰れません！！" },
  { name: "栗原遼", sex: "男", age: 24, text: "関西人です！！" },
];

const Home: FC = () => {
  return (
    <Fragment>
      <HomeComponent people={people} />
    </Fragment>
  );
};

export default Home;
